# Debianパッケージング アップロード編

subtitle
:  おすすめのアップロードツールはどれか？

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2023年1月 東京エリア・関西合同Debian勉強会

allotted-time
:  20m

theme
:  .

# スライドはRabbit Slide Showにて公開済みです

* Debianパッケージング アップロード編
  * <https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-dputng-howto-202301/>

# 本日の内容

* Debianパッケージのアップロードの機会
* 各種アップロードツールの紹介
  * dput,dpug-ng,dupload
* おすすめのツールについて

# Debianパッケージのアップロードの機会

* パッケージのメンテナンス
  * コントリビューターによるアップロード
    * 例: mentors.d.nへのアップロードなど
  * DM/DD: 既存のパッケージのメンテナンス
  * DD:新規パッケージのスポンサー
    * 例: ftp-masterへのスポンサーアップロードなど

# アップロードに用いられるツール

* dput (圧倒的に利用者が多い)
* dupload (dput-ngよりは使われている)
  * <https://www.debian.org/doc/manuals/maint-guide/upload.ja.html>
  * <https://www.debian.org/doc/manuals/developers-reference/tools.ja.html#package-uploaders>
* dput-ng (一部の人が使っている)

# dputのインストール動向

![](images/popcon-dput.png){:relative-height="80"}

* <https://qa.debian.org/popcon.php?package=dput>

# duploadのインストール動向

![](images/popcon-dupload.png){:relative-height="80"}

* <https://qa.debian.org/popcon.php?package=dupload>

# dput-ngのインストール動向

![](images/popcon-dput-ng.png){:relative-height="80"}

* <https://qa.debian.org/popcon.php?package=dput-ng>

# dputとは

* ほぼデファクトスタンダードのアップロードツール
  * devscriptsのrecommendsでインストールされる
  * packaging-devの依存でインストールされる
* ~/.dput.cfでユーザーごとにカスタマイズ
  * PPAにアップロードするときとか

# dput-ngとは(1)

* <https://dput.readthedocs.io/en/latest/>
  * dputとの設定ファイルの互換性を維持しつつも方向性が異なるプロジェクト
  * フックの仕組みでチェック機能を拡張可能
    * 例: 3rdパーティーが開発したチェッカーを利用できる
  * 独自書式の設定ファイルで挙動を柔軟に変えることもできる
  * DMのパーミッションの操作もできる(dcutの機能強化)
  
# dput-ngとは(2)

* **dputと併用はできない**
  * dput-ngをインストールするとdputは削除される
* ドキュメントはdputよく知っている前提
  * 利用可能なフックの説明はなく、実装についてのページがある
    * <https://dput.readthedocs.io/en/latest/library/hooks/index.html>
  * dput(1)もしくはdput(5)の参照おすすめ

# dirtを使ってフックを調べる

* dirt listコマンドが利用できる

```
% dirt list

WARNING: This command is not completed yet. Interface and behavior changes are expected in future releases

pre-upload hooks:
	required-fields: check whether a field is present and non-empty in the changes file
	checksum: verify checksums before uploading
	protected-distribution: warn before uploading to distributions where a special policy applies
	lintian: runs lintian before the upload
	gpg: check GnuPG signatures before the upload
	allowed-distribution: check whether a local profile permits uploads to the target distribution
	suite-mismatch: check the target distribution for common errors
	supported-distribution: check whether the target distribution is currently supported (using distro-info)
	check-debs: makes sure the upload contains a binary package

post-upload hooks:
	impatient: Tell me when dinstall will pick this up
```

# duploadとは

* dput,dput-ngと同様にアップロードするためのもの
  * dput,dput-ngよりは機能が少なめ
  * 設定(.dupload.conf)に従ってアナウンスメールを送信する機能あり
  * .uploadがあればスキップしたり、チェックサムの確認機能ぐらい

# 利用可能なチェックを比較する

* dputで利用可能なチェック: 7
* dput-ngで利用可能なチェック: 10
* duploadで利用可能なチェック: (省略)

# dputで利用可能なチェック(1)

* Unique upload
  * アップロードしたときのログがあればアップロードを中断する
* Required package files
  * 必要なファイルが含まれていないときにアップロードを中断する
* Successful local install
  * パッケージをインストールした状態でないときにアップロードを中断する(既定で無効)

# dputで利用可能なチェック(2)

* Cryptographic signature
  * GPG署名していないときにアップロードを中断する(既定で有効)
* Package files content match
  * サイズやハッシュ値が一致していないときにアップロードを中断する(既定で有効)

# dputで利用可能なチェック(3)

* Distribution name is permitted for the remote host
  * アップロード先が許容しないdistributionの場合にアップロードを中断する(サーバー側設定に依存)
* Lintian success
  * lintianが失敗したときにアップロードを中断する(既定で無効)

# dput-ngで利用できるフック(1)

* allowed-distribution
  * プロファイルに応じてアップロードしてもよいかチェックする
    * 例: debian/changelogでUNRELEASEDのままアップロードしようとしたら止める
  
* check-debsフック
  * ソース or バイナリどちらが含まれるべきかチェックする
    * 例: PPAにバイナリパッケージをアップロードしようとするとエラー

# dput-ngで利用できるフック(2)

* checksumフック
  * アップロード前のパッケージのチェックサムがあっているかをチェックする
* gpgフック
  * アップロード前にGnuPGで署名してあるかをチェックする
    * 署名していなかったら署名をうながし、アップロードできる

# dput-ngで利用できるフック(2)

* impatientフック
  * 次にdinstallが実行されるまでの時間を通知する

* lintianフック
  * アップロード前にlintianを実行する

# dput-ngで利用できるフック(3)

* protected-distributionフック
  * 特別なポリシーが適用されるところへのアップロード前に警告する
    * 例： testing-proposed-updates, proposed-updates, oldstable, stable, testing, stable-security, oldstable-security, testing-securityなどにアップロードしようとしたときに確認が求められる

# dput-ngで利用できるフック(4)

* required-fieldsフック
  * 指定された項目があるかをチェックする
    * 例：.changesファイルにプロファイルで定義したrequired-fieldsに指定されたものが含まれていないと警告する

# dput-ngで利用できるフック(4)

* suite-mismatchフック
  * changelogと.changesのディストリビューションが一致しない場合、警告する
    * 例: unstableでexperimentalが混在しているとき

* supported-distributionフック
  * .changesのDistributionがサポートされていないときに警告する
    * 例: サポートの有無はpython3-distro-infoを使って/usr/share/distro-info/debian.csvを参照する

# サンプルとして利用できるフック

* bd-blacklist
  * Build-Dependsに対象が含まれていたら失敗する
* clojure-arno-tester
  * メンテナーがarno@debian.orgだったら失敗する
* twitter
  * アップロード後にtweetする
  
# 各種ツールの機能比較(1)

|   |dupload|dput|dput-ng|
|---|---|---|---|
|提供コマンド|dupload|dput,dcut|dput,dcut,**dirt**|
|設定ファイル|~/.dupload.conf|~/.dput.cf or ~/.config/dput/dput.cf|~/.dput.cf or 独自|

# 各種ツールの機能比較(2)

|   |dupload|dput|dput-ng|
|フックの追加実装|❌非対応|❌非対応|✅対応|
|対応プロトコル|ftp,http,https,scp,scpb,**rsync**,copy|ftp,http,https,scp,**rsync**,local|ftp,http,https,scp,local|
|メール連携|✅対応|❌非対応|❌非対応|

# 各種ツールの機能比較(3)

|   |dupload|dput|dput-ng|
|DMの権限操作|❌非対応|❌非対応|✅dcut追加機能|

# dcutによるDMの権限操作

* dput-ng版ではdcutでDMの許可を変更できる

```
% dcut dm \
  --uid 66DEF15282990C2199EFA801A8A128A8AB1CEE49 \
  --allow zarchive
Uploading commands file to 
ftp.upload.debian.org (incoming: /pub/UploadQueue/)
Picking DM Andrea Pappacoda 
(Tachi's main key) <andrea@pappacoda.it> 
with fingerprint 66DEF15282990C2199EFA801A8A128A8AB1CEE49
Uploading kenhys-1673679146.dak-commands to ftp-master
```

# dput-ngでのプロファイルの考え方

* dputと互換性のないフォーマット(JSON)について
  * メタ情報を拡張したものがプロファイル
    * 例: debianというメタ情報からmentors-ftpプロファイルを派生
  * 基本的な設定をメタ情報として定義し、サイトごとの設定をプロファイルで指定して上書きするイメージ

# dput-ngでのプロファイル設定例(1)

* <https://mentors.debian.net/intro-maintainers/>
  * FTPの場合に~/.dput.cfに設定する内容

```
[mentors-ftp]
fqdn = mentors.debian.net
login = anonymous
progress_indicator = 2
passive_ftp = 1
incoming = /pub/UploadQueue/
method = ftp
allow_unsigned_uploads = 0
# Allow uploads for UNRELEASED packages
allowed_distributions = .*
```

# dput-ngでのプロファイル設定例(2)

* /etc/dput.d/profiles/mentors.json

```
{
    "-hooks": [
        "check-debs",
        "allowed-distribution"
    ],
    "allowed_distributions": ".*",
    "fqdn": "mentors.debian.net",
    "incoming": "/pub/UploadQueue/",
    "login": "anonymous",
    "meta": "debian",
    "method": "ftp"
}
```


# dput-ngでのプロファイル設定例(3)

* 継承元の/etc/dput.d/metas/debian.json定義

 ```
{
    "allow_dcut": true,
    "allowed-distribution": {
        "codename-groups": [
            "general", "backport", "rm-managed"
        ]
    },
    "codenames": "debian",
    "hooks": [
        "allowed-distribution",
        "protected-distribution",
        "checksum",
        "suite-mismatch",
        "gpg"
    ],
    ...(省略)...
}
```


# 結局、どれを使うのがよいのか？ (1)

* dupload
  * ✅メール連携が使いたい
  * ✅シンプルな機能で十分派
  * ❌チェック機能は少なめ

# 結局、どれを使うのがよいのか？ (2)

* dput
  * ✅メジャーなものを使いたい派
  * ✅チェックはdputのもので十分派
  * ❌DMの権限変更もしたい
  
# 結局、どれを使うのがよいのか？ (3)

* dput-ng
  * ✅チェック機能を強化したい派
  * ✅アップロード時に署名もまとめて実行したい
  * ✅DMの権限変更もしたい
  * ❌rsyncでアップロード必須

# さいごに

* dputとdput-ng、設定ファイルの互換性あるものの機能性に違いあり
* アップロード時に署名もできるのでdput-ngオススメ

# 参考資料(1)

* <https://manpages.debian.org/unstable/dput/index.html>
  * dput(1)
  * dcut(1)
  * dput.cf(5)

# 参考資料(2)

* <https://manpages.debian.org/unstable/dput-ng/index.html>
  * dput(1)
  * dput(5) dput-ngの独自設定フォーマットについて知るのによい
  * dcut(1) dmサブコマンドの使い方について
  * dput.cf(5)

